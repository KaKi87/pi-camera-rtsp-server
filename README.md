# pi-camera-rtsp-server

Raspberry Pi camera RTSP server tested on Pi Zero W using OV5647 module.

## Requirements

Hardware :
- Raspberry Pi
- Raspberry Pi compatible camera
- Pi/camera compatible ribbon cable

System : up-to-date Raspbian

Setting : camera interface enabled from `raspi-config`

Software :
- `bash`
- `nodejs`
- `raspivid`
- `cvlc` from `vlc`
- `openRTSP` from `livemedia-utils`

No NodeJS dependancies.

## API

### Camera

#### `camera.config`

Settings `Object`

- `port` : RTSP server broadcasting port
<br>*Default `8554`*
- `rotation` : image rotation
<br>*Default `0`, available `90` `180` `270` (i.e. -90)*

#### `camera.getStatus()`

Checks if camera feed is active by the following conditions :
- `raspivid` (unique) process active
- RTSP server port open

Returns `Promise<Status{}>`

`Status` object :
- `enabled` *(boolean)* camera enabled
- `manually` *(boolean)* camera enabled manually, without API
<br>Means camera cannot be properly disabled from API.

#### `camera.enable()`

Enable camera. Returns `Promise`

#### `camera.disable()`

Disable camera.
<br>All active records will be stopped first.
<br>Returns `Promise`

### Record

#### `new Record(file, overwrite)`

- `file` *(string)* : file path + name.
<br>**Absolute path required** ; use `$HOME` instead of `~` when applicable.
- `overwrite` *(boolean)* : delete duplicate if exists.

#### `Record.start()`

Start record.
<br>Camera will be enabled if not already active.
<br>Returns `Promise`

#### `Record.stop()`

Stop record.
<br>Returns `Promise`

#### `Record.file`

Record file path + name

#### `Record.duration`

Record duration

#### `Record.pid`

Record process PID *(integer)*

#### `Record.startTime`

Record start time (`Date`)

#### `Record.stopTime`

Record stop time (`Date`)

#### `Record.elapsedTime`

Record elapsed time in *(integer, seconds)*

#### `Record.recording`

Record active state *(boolean)*

#### `static Record.startAll()`

Start all non-started records.
<br>Returns `Promise`

#### `static Record.stopAll()`

Stop all active records.
<br>Returns `Promise`

#### `static Record.records`

All records *(array of `Record` instances)*

## Unit testing

- Make sure you have `ffprobe` binary from `ffmpeg` package
- Install development dependancies : `yarn install`
- Fill `test/config.json` with your video recording test folder
- Disable camera *(if enabled)*
- Run `yarn test`

## Planned features

- pause/resume recording *([using video file concatenation with ffmpeg demuxer method](https://stackoverflow.com/a/11175851))*
- enhance camera status detection *(e.g. in use by another executable than `raspivid`)*

## License

This project is licensed under GNU GPL v3.