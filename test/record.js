const
	chai = require('chai'),
	expect = require('chai').expect,
	fs = require('fs'),
	PQueue = require('p-queue').default,
	findRemove = require('find-remove');

chai.use(require('chai-as-promised'));
chai.use(require('chai-string'));

const { camera, Record } = require('..');

let
	duplicateFilePath,
	rec,
	recs,
	queue;

describe('record', () => {
	before('enable camera if needed', done => { camera.enable().then(done).catch(() => done()) });
	describe('file/path errors', () => {
		it('should refuse non-absolute file path', () => expect(() => new Record('test.mp4')).to.throw('ABSOLUTE_PATH_REQUIRED'));
		it('should force file extension', () => expect(new Record('/test').file).to.endWith('.mp4'));
		describe('duplicate file handling', () => {
			before('define duplicate file path', () => duplicateFilePath = __dirname + '/test_duplicate.mp4');
			before('create duplicate file', () => fs.openSync(duplicateFilePath, 'w'));
			it('should refuse duplicate without overwrite parameter', () => expect(() => new Record(duplicateFilePath)).to.throw('FILE_EXISTS'));
			it('should overwrite duplicate with proper parameter', () => expect(new Record(duplicateFilePath, true)).to.be.an.instanceOf(Record));
		});
		after('clear Record instances', () => Record.records.length = 0);
	});
	describe('standard process', () => {
		before('define rec', () => rec = new Record(__dirname + '/test.mp4'));
		it('should start', () => expect(rec.start()).to.be.fulfilled);
		it('should have a PID', () => expect(isNaN(rec.pid)).to.be.false);
		it('should have a start time', () => expect(rec.startTime).to.be.an.instanceOf(Date));
		it('should have elapsed time', () => expect(isNaN(rec.elapsedTime)).to.be.false);
		it('should be recording', () => expect(rec.recording).to.be.true);
		it('should not start while already started', () => expect(rec.start()).to.be.eventually.rejectedWith('ALREADY_STARTED'));
		it('should stop', done => setTimeout(() => rec.stop().then(done).catch(done), 7000));
		it('should have stopped recording', () => expect(rec.recording).to.be.false);
		it('should have a stop time', () => expect(rec.stopTime).to.be.an.instanceOf(Date));
		it('should have a duration', () => expect(isNaN(rec.duration)).to.be.false);
		it('should have been accurately timed', () => expect(Math.abs(rec.elapsedTime - rec.duration) <= 2.5).to.be.true);
		it('should not stop while already stopped', () => expect(rec.stop()).to.be.eventually.rejectedWith('ALREADY_STOPPED'));
		it('should not resume', () => expect(rec.start()).to.be.eventually.rejectedWith('RESUME_UNAVAILABLE'));
	});
	describe('multiple instances', () => {
		/*
		0 1 2 3
		0 1     - start sequentially
			2 3 - start simultaneously
		0   2   - stop  sequentially
		  1   3 - stop  simultaneously
		 */
		before('define recs & queue', () => {
			queue = new PQueue({ concurrency: 1 });
			recs = Array.from(Array(4), (_, i) => new Record(`${__dirname}/test_multiple_${i}.mp4`));
		});
		it('should start sequentially', () => expect(queue.addAll([recs[0], recs[1]].map(rec => () => rec.start()))).to.be.fulfilled);
		it('should start simultaneously', () => expect(Record.startAll()).to.be.fulfilled);
		it('should stop sequentially', () => expect(queue.addAll([recs[0], recs[1]].map(rec => () => rec.stop()))).to.be.fulfilled);
		it('should stop simultaneously', () => expect(Record.stopAll()).to.be.fulfilled);
	});
	describe('automated camera/recorder interaction', () => {
		before('redefine rec and disable camera if needed', done => {
			rec = new Record(__dirname + '/test_auto.mp4');
			camera.disable().then(done).catch(() => done())
		});
		it('should start record', () => expect(rec.start()).to.be.fulfilled);
		it('should have enabled camera', () => expect(camera.getStatus()).to.eventually.eql({ enabled: true, manually: false }));
		it('should disable camera', () => expect(camera.disable()).to.be.fulfilled);
		// it('should have stopped recording', () => expect(rec.recording).to.be.false);
		it('should have stopped recording', () => expect(new Promise((resolve, reject) => {
			const _reject = setTimeout(reject, 2000);
			const check = setInterval(() => {
				if(rec.stopTime instanceof Date){
					clearTimeout(_reject);
					clearInterval(check);
					resolve();
				}
			}, 500);
		})).to.be.fulfilled);
	});
	after('delete all test files and disable camera if needed', done => {
		findRemove(__dirname, { extensions: ['.mp4'] });
		camera.disable().then(done).catch(() => done());
	});
});