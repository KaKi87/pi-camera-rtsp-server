const
	chai = require('chai'),
	expect = require('chai').expect;

chai.use(require('chai-as-promised'));

const { camera } = require('..');

describe('camera', () => {
	before('disable camera', done => {
		// TODO test
		camera.disable()
			.then(done)
			.catch(err => {
				if(err === 'ALREADY_DISABLED') done();
				else throw new Error('UNIT_TESTING_CAMERA_DEACTIVATION_REQUIRED');
			});
	});
	describe('standard process', () => {
		it('should be disabled', () => expect(camera.getStatus()).to.eventually.eql({ enabled: false, manually: false }));
		it('should enable', () => expect(camera.enable()).to.be.fulfilled);
		it('should be enabled', () => expect(camera.getStatus()).to.eventually.eql({ enabled: true, manually: false }));
		it('should not enable while already enabled', () => expect(camera.enable()).to.be.eventually.rejectedWith('ALREADY_ENABLED'));
		it('should disable', () => expect(camera.disable()).to.be.fulfilled);
		it('should have been disabled', () => expect(camera.getStatus()).to.eventually.eql({ enabled: false, manually: false }));
		it('should not disable while already disabled', () => expect(camera.disable()).to.be.eventually.rejectedWith('ALREADY_DISABLED'));
	});
	describe('manual activation', () => {
		let pid;
		it('should enable manually', () => expect(new Promise((resolve, reject) => {
			require('child_process')
				.spawn('bash', ['../lib/camera/start.sh'], { cwd: __dirname })
				.stdout.once('data', data => {
				pid = parseInt(data.toString().trim());
				if(!isNaN(pid)) resolve();
				else reject();
			});
		})).to.be.fulfilled);
		it('should be manually enabled', () => expect(new Promise((resolve, reject) => {
			const _reject = setTimeout(reject, 5000);
			const check = () => {
				camera.getStatus().then(state => {
					if(state.enabled && state.manually){
						clearTimeout(_reject);
						resolve();
					}
					else
						setTimeout(check, 500);
				});
			};
			check();
		})).to.be.fulfilled);
		it('should not enable while manually enabled', () => expect(camera.enable()).to.be.eventually.rejectedWith('MANUALLY_ENABLED'));
		it('should not disable while manually enabled', () => expect(camera.disable()).to.be.eventually.rejectedWith('MANUALLY_ENABLED'));
		it('should disable manually', () => expect(process.kill(pid)).to.be.true);
	});
	after('disable camera if needed', done => {
		camera.disable().then(done).catch(() => done());
	});
});