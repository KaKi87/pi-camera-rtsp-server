#!/bin/bash
PORT=${1:-8554}
PID=$2
if [ "$(pidof raspivid | wc -l)" -eq 1 ] && [ "$(netstat -a | grep -c 0.0.0:"$PORT")" -eq 1 ]; then
	# raspivid active and rtsp port open
	if [ -z "$PID" ]; then
		# pid not provided
		echo 2
	else
		# pid provided
		if [ "$(ps -p "$PID" -o comm=)" == "vlc" ]; then
			# pid is a vlc process
			echo 1
		else
			# pid is not a vlc process
			echo 3
		fi
	fi
else
	# no raspivid or port closed
	echo 0
fi