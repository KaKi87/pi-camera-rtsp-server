const Record = require('../Record');

const config = {
    rotation: 0,
    width: 1440,
    height: 1080,
    bitrate: 15000000,
    framerate: 15,
    port: 8554
};

let pid;

const getStatus = () => new Promise((resolve, reject) => {
    require('..').exec(`bash isActive.sh ${config.port} ${isNaN(pid) ? '' : pid}`, __dirname).then(res => {
        res = parseInt(res.toString());
        resolve({
            enabled: res > 0,
            manually: res > 2
        });
    }).catch(reject);
});

const enable = () => new Promise((resolve, reject) => getStatus().then(state => {
    if(state.enabled) return reject(state.manually ? 'MANUALLY_ENABLED' : 'ALREADY_ENABLED');
    const script = require('child_process').spawn('bash', ['start.sh', ...Object.values(config)], { cwd: __dirname });
    script.stdout.once('data', data => {
        pid = data.toString().trim();
        const waitForActivation = () => {
            getStatus().then(state => {
                if(state.enabled && !state.manually) return resolve();
                waitForActivation();
            });
        };
        waitForActivation();
    });
    script.stderr.once('data', res => reject(res.toString().trim()));
}));

const disable = () => new Promise((resolve, reject) => getStatus().then(state => {
    if(!state.enabled) return reject('ALREADY_DISABLED');
    if(state.manually) return reject('MANUALLY_ENABLED');
    Record.stopAll();
    try {
        process.kill(pid);
        resolve();
    }
    catch(err) {
        reject(err);
    }
}));

module.exports = {
    config,
    getStatus,
    enable,
    disable
};