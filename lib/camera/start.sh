#!/bin/bash
ROTATION=${1:-0}
WIDTH=${2:-1440}
HEIGHT=${3:-1080}
BITRATE=${4:-15000000}
FRAMERATE=${5:-15}
PORT=${6:-8554}
raspivid -n -a 1032 -a "%d/%m/%y %X" -rot "$ROTATION" -w "$WIDTH" -h "$HEIGHT" -b "$BITRATE" -fps "$FRAMERATE" -t 0 -o - | cvlc -v stream:///dev/stdin --rtsp-timeout=99999 --sout "#rtp{sdp=rtsp://:$PORT/}" :demux=h264 :h264-fps="$FRAMERATE" &>/dev/null &
echo $!