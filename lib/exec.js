module.exports = (command, directory) => new Promise((resolve, reject) => require('child_process').exec(command, {
    cwd: directory || undefined
}, (error, stdout, stderr) => {
    // if(error) return reject(error);
    if(stderr) return reject(stderr.trim());
    resolve(stdout.trim());
}));