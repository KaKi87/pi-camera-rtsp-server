#!/bin/bash
FILE=${1:-$HOME/$(date +%Y-%m-%d_%H-%M-%S).mp4}
WIDTH=${2:-1440}
HEIGHT=${3:-1080}
FRAMERATE=${4:-15}
PORT=${5:-8554}
openRTSP -4 -w "$WIDTH" -h "$HEIGHT" -f "$FRAMERATE" "rtsp://0.0.0.0:$PORT/" > "$FILE" 2>/dev/null &
echo $!