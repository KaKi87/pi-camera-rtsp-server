const fs = require('fs');

const exec = command => require('..').exec(command, __dirname);

class Record {
    constructor(file, overwrite){
        if(!file.startsWith('/')) throw new Error('ABSOLUTE_PATH_REQUIRED');
        if(!file.endsWith('.mp4')) file += '.mp4';
        if(fs.existsSync(file) && !overwrite) throw new Error('FILE_EXISTS');
        const duplicate = Record.records.find(record => record.file === file && record.startTime && !record.stopTime);
        if(duplicate) duplicate.stop();
        this.file = file;
        Record.records.push(this);
    }
    start(){
        return new Promise((resolve, reject) => {
            if(this.startTime) return reject(this.stopTime ? 'RESUME_UNAVAILABLE' : 'ALREADY_STARTED');
            const _start = () => {
                const config = require('..').camera.config;
                exec(`bash start.sh ${this.file} ${config.width} ${config.height} ${config.framerate} ${config.port}`).then(pid => {
                    this.pid = parseInt(pid.toString());
                    const wait = setInterval(() => {
                        if(this.recording){
                            clearInterval(wait);
                            this.startTime = new Date();
                            resolve();
                        }
                    }, 0);
                }).catch(reject);
            };
            require('../camera').enable().then(_start).catch(_start);
        });
    }
    stop(){
        return new Promise((resolve, reject) => {
            if(!this.startTime) reject('NOT_STARTED');
            if(this.stopTime) reject('ALREADY_STOPPED');
            try {
                process.kill(this.pid, 'SIGHUP');
                this.stopTime = new Date();
                exec('bash duration.sh ' + this.file).then(duration => {
                    this.duration = parseFloat(duration.toString());
                    resolve();
                }).catch(reject);
            }
            catch(err) {
                reject(err.code === 'ESRCH' ? 'MANUALLY_STOPPED' : err);
            }
        });
    }
    get elapsedTime(){
        return this.startTime ? ((this.stopTime || new Date()) - this.startTime) / 1000 : undefined;
    }
    get recording(){
        if(!fs.existsSync(this.file) || this.stopTime) return false;
        if(this.elapsedTime < 1000) return true;
        return new Date() - fs.statSync(this.file).mtime < 150;
    }
}

Record.records = [];

Record.startAll = () => Promise.all(Record.records.filter(record => !record.startTime).map(record => record.start()));

Record.stopAll = () => Promise.all(Record.records.filter(record => record.recording === true).map(record => record.stop()));

module.exports = Record;